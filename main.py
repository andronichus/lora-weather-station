#! MicroPython on Pycom LoPy
#
# Send accumlated and selected data from Fine Offset RS495 weather station via LoRaWAN.
# See https://doughall.me/2018/03/30/ctypes-and-uctypes/
#
# Copyright (C) 2018, Doug Hall
# Licensed under MIT license, see included file LICENSE or http://opensource.org/licenses/MIT
#
# TODO
# - change to micropython logging()
# - turn off wifi after start
# - reset rain accumulation
# - work out why garbage collection isn't working
#
from network import WLAN
import gc
import time
import pycom
import socket
import machine
import ujson
import uctypes
from ustruct import pack, pack_into
from network import LoRa
from binascii import unhexlify, hexlify
from uwdata import RawWeatherData

# Configuration and constants
CONFIG          = 'config.json'
uartnum         = 1
RGB_OFF         = 0x000000
RGB_DATA        = 0x403000
RGB_NO_DATA     = 0x400000
RGB_TRANSMIT    = 0x004000
RGB_LORA_JOIN   = 0x000040
RGB_LORA_JOINED = 0x004000
LED_TIMEOUT     = 0.2
sock            = 0
PERIOD          = 16        # seconds between data gathering for the wakeup timer
CYCLE           = 10        # number of cycles to accumulate data
triggercount    = 0         # current loop count of accumulation and averaging
stored          = []

def log(msg):
    pass
#    print('fo485: {}'.format(msg))

# Read configuration data from CONFIG file
def setup():
    with open(CONFIG, 'r') as f:
        lorakeys = ujson.load(f)
    return(lorakeys)

# Open and initialise LoRa communications
def init_lora(cfg):
    dev_eui = unhexlify(cfg["deveui"].replace(' ',''))
    app_eui = unhexlify(cfg["appeui"].replace(' ',''))
    app_key = unhexlify(cfg["appkey"].replace(' ',''))
    lora = LoRa(mode=LoRa.LORAWAN)
    log('[Lora] Initializing LoRaWAN, DEV EUI: {} ...'.format(hexlify(lora.mac())
        .decode('ascii').upper()))
    if not app_key:
        log('[Lora] ERROR: LoRaWAN APP KEY not set!')
        return (None, None)
    pycom.rgbled(RGB_LORA_JOIN)
    lora.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0)
    while not lora.has_joined():
        log('[Lora] Joining...')
        pycom.rgbled(RGB_OFF)
        time.sleep(LED_TIMEOUT)
        pycom.rgbled(RGB_LORA_JOIN)
        time.sleep(2.5)
    pycom.rgbled(RGB_OFF)
    sock = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
    sock.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)      # Set data rate
    sock.setblocking(False)
    sock.bind(3) # send on port 3
    lora.nvram_save() # store LoRa details to
    log('[Lora] Joined')
    # turn off wifi
#    wlan = WLAN()
#    wlan.deinit()
    return (lora, sock)

# Read and interpret comms protocol on the RS485 line, and save into a dictionary
def interpret(pbytes):
    reading={}
    WD = uctypes.struct(uctypes.addressof(pbytes), RawWeatherData, uctypes.BIG_ENDIAN)
    reading['DIR']  = (WD.DIR8<<8) + WD.DIR
    reading['BAT']  = WD.BAT
    reading['TMP']  = (WD.TMP-400) / 10
    reading['HM']   = WD.HM
    reading['WIND'] = (WD.WSP8<<8) + WD.WIND
    reading['GUST'] = WD.GUST
    reading['RAIN'] = WD.RAIN
    reading['UVI']  = WD.UVI
    reading['LUX']  = WD.LIGHT
    log('[Intp] {}'.format(reading))
    return (reading)

# Average readings or take maximum for highest reading in last interval
def accumulate(momentary):
    store = {}
    store['DIR']  = int(sum(d['DIR'] for d in momentary) / len(momentary))
    store['BAT']  = max(d['BAT'] for d in momentary)
    store['TMP']  = int(max(d['TMP'] for d in momentary))
    store['HM']   = max(d['HM'] for d in momentary)
    store['WIND'] = int(sum(d['WIND'] for d in momentary) / len(momentary))
    store['GUST'] = max(d['GUST'] for d in momentary)
    store['RAIN'] = max(d['RAIN'] for d in momentary)
    store['UVI']  = max(d['UVI'] for d in momentary)
    store['LUX']  = max(d['LUX'] for d in momentary)
#    log('[Stor] {}'.format(store))
    return(store)

# Transmit data using LoRaWAN
def transmit(wdata):
    message = bytes(wdata)
    count = sock.send(wdata)
    pycom.rgbled(RGB_TRANSMIT)
    log('[Sent] {} ({} bytes)'.format(hexlify(message).upper(), count))

# Periodic task to read UART
def update_task(alarmtrigger):
    global triggercount, stored  # have to use globals as alarm trigger does not allow passing in locals
    datapoint = []

    # Read weather station data
    weatherdata = uart.read(17)
#    log('[Data] {}'.format(weatherdata))
    if weatherdata:
        pycom.rgbled(RGB_DATA)
        datapoint = interpret(weatherdata)
    else:
        pycom.rgbled(RGB_NO_DATA)

    # Accumulate/average on each cycle
    triggercount += 1
    if triggercount < CYCLE:
        stored.append(datapoint)
#        log('[Stor] {}'.format(stored))
    else:
        # Once accumulated, transmit data and clear storage
        final = accumulate(stored)
        log('[Finl] {}'.format(final))
        td=pack('!HBBBBBHHL',final['DIR'],final['BAT'],final['TMP'],
            final['HM'],final['WIND'],final['GUST'],
            final['RAIN'],final['UVI'],final['LUX'])
        log('[Pack] {}'.format(td))
        transmit(td)
        triggercount = 0
        stored = []

    pycom.rgbled(RGB_OFF)
    gc.collect()
    machine.idle()

# Main program
log('Start')
pycom.heartbeat(False)
cfg=setup()
gc.enable()
uart = machine.UART(uartnum, baudrate=9600, timeout_chars=2000)
(lora, sock) = init_lora(cfg)
if lora:
    weather = machine.Timer.Alarm(update_task, s=PERIOD, periodic=True)
log('Start completed')
