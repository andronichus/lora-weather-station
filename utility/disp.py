#!/usr/bin/env python3
#
# disp.py - read from Fine Offset RS495 weather station.
# Take RS485 via USB message from a Fine Offset WH2950 and interpret.
# See https://wordpress.com/post/doughall.me/1683
#
# Copyright (C) 2018, Doug Hall
# Licensed under MIT license, see included file LICENSE or http://opensource.org/licenses/MIT
#
import serial 
import binascii
from wdata import RawWeatherData, wdata

# For testing we can either use an actual raw format message embedded here in the code, or read from the USB input.

def main():
    #b = bytearray(b'24b3626a6b390000000800040000003d90')
    #b = bytearray(b'$\xb3Xj_:\x00\x00\x00D\x00\x00\x00\x00\x00\xb8.')
    s = serial.Serial('/dev/ttyUSB0', 9600)

    wd = wdata()
    while True:
        b = s.read(17)
        wd = wdata.from_buffer_copy(b)
        print(binascii.hexlify(bytearray(b)))
        #print("fc: {}".format(wd.rawdata.FC))
        #print("sc: {}".format(wd.rawdata.SC))
        print("dir: {}".format((wd.rawdata.DIR8<<8)+wd.rawdata.DIR))
        #print(wd.rawdata.FIX)
        print("bat: {}".format(wd.rawdata.BAT))
        print("tmp: {}".format((wd.rawdata.TMP-400)/10))
        print("hm: {}".format(wd.rawdata.HM))
        print("wind: {}".format((wd.rawdata.WSP8<<8)+wd.rawdata.WIND))
        print("gust: {}".format(wd.rawdata.GUST))
        print("rain: {}".format(wd.rawdata.RAIN))
        print("uvi: {}".format(wd.rawdata.UVI))
        print("light: {}".format(wd.rawdata.LIGHT))
        #print("crc: {}".format(wd.rawdata.CRC))
        #print("checksum: {}".format(wd.rawdata.CHECKSUM))
        #print("--------------------------------")
        #x=crc8()
        #print("crc: {}".format(x.crc(b[0:14])))
        #print("sum: {}".format(sum(bytearray(b[0:16]))))
        print("================================")
        #return
    ser.close()

if __name__ == '__main__':
    main()
