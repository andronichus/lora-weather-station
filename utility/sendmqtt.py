#! MicroPython
#
# sendmqtt.py - send MQTT from Fine Offset RS495 weather station.
# Take RS485 message from a Fine Offset WH2950 and send via LoRaWAN. Note: this is NOT done by actually transmitting MQTT from the device,
# but by sending via LoRaWAN to TTN and then using TTN's MQTT forwarding.
# See https://wordpress.com/post/doughall.me/1683
#
# Copyright (C) 2018, Doug Hall
# Licensed under MIT license, see included file LICENSE or http://opensource.org/licenses/MIT
#
import gc
import time
import array
import pycom
import socket
import machine
import ujson
from network import LoRa
from binascii import unhexlify, hexlify

CONFIG          = '../config.json'
uartnum         = 1
SEND_RATE       = 16
RGB_OFF         = 0x000000
RGB_DATA        = 0x403000
RGB_NO_DATA     = 0x400000
RGB_TRANSMIT    = 0x004000
RGB_LORA_JOIN   = 0x000040
RGB_LORA_JOINED = 0x004000
LED_TIMEOUT     = 0.2
sock            = 0

def log(msg):
    print('sendmqtt: {}'.format(msg))

def setup():
    with open(CONFIG, 'r') as f:
    	lorakeys = json.load(f)
    return(lorakeys)

def init_lora(cfg):
    dev_eui = unhexlify(cfg["deveui"].replace(' ',''))
    app_eui = unhexlify(cfg["appeui"].replace(' ',''))
    app_key = unhexlify(cfg["appkey"].replace(' ',''))
    lora = LoRa(mode=LoRa.LORAWAN)
    log('Initializing LoRaWAN, DEV EUI: {} ...'.format(hexlify(lora.mac())
        .decode('ascii').upper()))
    if not app_key:
        log('ERROR: LoRaWAN APP KEY not set!')
        return (None, None)
    pycom.rgbled(RGB_LORA_JOIN)
    lora.join(activation=LoRa.OTAA, auth=(dev_eui, app_eui, app_key), timeout=0)
    while not lora.has_joined():
        log('Joining...')
        pycom.rgbled(RGB_OFF)
        time.sleep(LED_TIMEOUT)
        pycom.rgbled(RGB_LORA_JOIN)
        time.sleep(2.5)
    pycom.rgbled(RGB_OFF)
    sock = socket.socket(socket.AF_LORA, socket.SOCK_RAW)
    sock.setsockopt(socket.SOL_LORA, socket.SO_DR, 5)
    sock.setblocking(False)
    log('Joined!')
    return (lora, sock)

def transmit(wdata):
    message = bytes(wdata)
    count = sock.send(message)
    pycom.rgbled(RGB_TRANSMIT)
    log('Message sent: {} ({} bytes)'.format(hexlify(message).upper(), count))

def update_task(alarmtrigger):
    time.sleep(LED_TIMEOUT)
    weatherdata = uart.read(17)
    if weatherdata:
        pycom.rgbled(RGB_DATA)
    else:
        pycom.rgbled(RGB_NO_DATA)
    transmit(weatherdata)
    time.sleep(LED_TIMEOUT)
    pycom.rgbled(RGB_OFF)
    gc.collect()
    machine.idle()

# Main program
log('Starting')
pycom.heartbeat(False)
cfg=setup()
gc.enable()
uart = machine.UART(uartnum, baudrate=9600, timeout_chars=2000)
(lora, sock) = init_lora(cfg)
if lora:
    weather = machine.Timer.Alarm(update_task, s=SEND_RATE, periodic=True)
log('Startup completed')
