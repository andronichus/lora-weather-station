#!/usr/bin/env python3
#
# readmqtt.py - reads raw data message from The Things Network and displays on screen.
# See https://doughall.me/2018/03/30/ctypes-and-uctypes/
#
# Copyright (C) 2018, Doug Hall
# Licensed under MIT license, see included file LICENSE or http://opensource.org/licenses/MIT
#
import base64
import json
import curses
import paho.mqtt.client as mqtt
from wdata import RawWeatherData, wdata

CONFIG = '../config.json'

def setup():
    with open(CONFIG, 'r') as f:
    	lorakeys = json.load(f)
    return(lorakeys)

def on_connect(client,userdata,flags,rc):
    stdscr.addstr(1,1,"Conn: "+str(rc))
    client.subscribe('+/devices/+/up')
    stdscr.refresh()

def on_message(client,userdata,msg):
    j = json.loads(msg.payload)
    pr = j['payload_raw']
    b = base64.b64decode(pr)
    wd = wdata.from_buffer_copy(b)
    stdscr.addstr(2,1,"dir : {}".format((wd.rawdata.DIR8<<8)+wd.rawdata.DIR))
    stdscr.addstr(3,1,"bat : {}".format(wd.rawdata.BAT))
    stdscr.addstr(4,1,"tmp : {}".format((wd.rawdata.TMP-400)/10))
    stdscr.addstr(5,1,"hm  : {}".format(wd.rawdata.HM))
    stdscr.addstr(6,1,"wind: {}".format((wd.rawdata.WSP8<<8)+wd.rawdata.WIND))
    stdscr.addstr(7,1,"gust: {}".format(wd.rawdata.GUST))
    stdscr.addstr(8,1,"rain: {}".format(wd.rawdata.RAIN))
    stdscr.addstr(9,1,"uvi : {}".format(wd.rawdata.UVI))
    stdscr.addstr(10,1,"lux : {}".format(wd.rawdata.LIGHT))
    stdscr.refresh()

cfg = setup()
stdscr = curses.initscr()

mqttc= mqtt.Client()
mqttc.on_connect=on_connect
mqttc.on_message=on_message
mqttc.username_pw_set(cfg['appid'],cfg['acckey'])
mqttc.connect("eu.thethings.network",1883,10)

def main(stdscr):
	while True:
		mqttc.loop()

curses.wrapper(main)
