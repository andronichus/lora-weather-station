#!/usr/bin/env python3
#
# readaccum.py - takes data sent through the The Things Network by subscribing on appropriate MQTT topics
# and then displays this on screen. used during testing to confirm transmission and decoding of values
# See https://doughall.me/2018/03/30/ctypes-and-uctypes/
#
# Copyright (C) 2020, Doug Hall
# Licensed under MIT license, see included file LICENSE or http://opensource.org/licenses/MIT
#
# To be done:
#  - DIR value sometimes is wrong, is this because of transmission or something to do with endianess?
#  -
#

import base64
import json
import sys
import curses
from struct import unpack, unpack_from
import paho.mqtt.client as mqtt
#from wdata import RawWeatherData, wdata
from collections import namedtuple

CONFIG = '../config.json'

def setup():
    with open(CONFIG, 'r') as f:
        lorakeys = json.load(f)
    return(lorakeys)

def on_connect(client,userdata,flags,rc):
#    print("conn: "+str(rc))
    stdscr.addstr(1,1,"Conn: "+str(rc))
    stdscr.refresh()

def on_message(client,userdata,msg):
#    print(msg.topic + " " + str(msg.qos))
    j = json.loads(msg.payload)
    pr = j['payload_raw']
    stdscr.addstr(1,10,"Raw: "+str(pr))
#    stdscr.addstr(1,40,"Raw: "+base64.b64decode(pr))
    stdscr.refresh()
##############################################################
# this is what is sent from the remote end (in MicroPython):
#   td=pack('!HBBBBBHHL',final['DIR'],final['BAT'],final['TMP'],
#     final['HM'],final['WIND'],final['GUST'],
#     final['RAIN'],final['UVI'],final['LUX'])
##############################################################
    wd = namedtuple('wd', ['dir','bat','tmp','hm','wind','gust','rain','uvi','lux'])
    weather=wd._make(unpack('!HBBBBBHHL',base64.b64decode(pr)))
    stdscr.addstr(2,1,"dir : {}".format(weather.dir))
    stdscr.addstr(3,1,"bat : {}".format(weather.bat))
    stdscr.addstr(4,1,"tmp : {}".format(weather.tmp))
    stdscr.addstr(5,1,"hm  : {}".format(weather.hm))
    stdscr.addstr(6,1,"wind: {}".format(weather.wind))
    stdscr.addstr(7,1,"gust: {}".format(weather.gust))
    stdscr.addstr(8,1,"rain: {}".format(weather.rain))
    stdscr.addstr(9,1,"uvi : {}".format(weather.uvi))
    stdscr.addstr(10,1,"lux : {}".format(weather.lux))
    stdscr.refresh()

def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))

def on_log(mqttc, obj, level, string):
    print(string)

def readloop(stdscr):
	while True:
		mqttc.loop()

stdscr = curses.initscr()
cfg = setup()
mqttc= mqtt.Client()
mqttc.on_connect=on_connect
mqttc.on_message=on_message
#mqttc.on_subscribe = on_subscribe
#mqttc.on_log = on_log
mqttc.username_pw_set(cfg['appid'],cfg['acckey'])
mqttc.connect("eu.thethings.network",1883,10)
mqttc.subscribe("+/devices/+/up")

#mqttc.loop_forever()
curses.wrapper(readloop)
curses.endwin()
